The EnthoughtBase package, a part of the Enthought Tool Suite.

This package provides a central mechanism for configuring the behavior
of various packages in ETS.  Libraries and applications can import ETSConfig
from enthought.etsconfig.api and get access to information such as 
the user's preferred UI toolkit (e.g. wx, qt4, or null), the base directory
for storing user data, the application installation direction, etc.

