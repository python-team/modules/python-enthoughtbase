The icons are mostly derived work from other icons. As such they are
licensed accordingly to the original license:

Project                 License                   File
----------------------------------------------------------------------------
CP (Crystal Project)    LGPL                      image_LICENSE_CP.txt

Unless stated in this file, icons are the work of Enthought, and are
released under a 3 clause BSD license.

Files and original authors:
----------------------------------------------------------------------------
enthought/logger/widget/images:
                          about.png | CP
                     bug_yellow.png | CP
                     crit_error.png | CP
                          error.png | CP
                        warning.png | CP
