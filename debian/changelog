python-enthoughtbase (3.1.0-2.1) UNRELEASED; urgency=low

  [ Stefano Rivera ]
  * Bump X-Python-Version to >= 2.5. Uses absolute_import __future__ imports.

  [ Jakub Wilk ]
  * Use canonical URIs for Vcs-* fields.

  [ Ondřej Nový ]
  * Fixed homepage (https)
  * Fixed VCS URL (https)
  * d/control: Set Vcs-* to salsa.debian.org
  * d/control: Remove ancient X-Python-Version field
  * Remove debian/pycompat, it's not used by any modern Python helper
  * Convert git repository from git-dpm to gbp layout
  * d/control: Update Maintainer field with new Debian Python Team
    contact address.
  * d/control: Update Vcs-* fields with new Debian Python Team Salsa
    layout.

 -- Jakub Wilk <jwilk@debian.org>  Sun, 05 May 2013 16:02:32 +0200

python-enthoughtbase (3.1.0-2) unstable; urgency=low

  * Team upload.
  * Rebuild to pick up 2.7 and drop 2.5 from supported versions
  * d/control: Bump Standards-Version to 3.9.2

 -- Sandro Tosi <morph@debian.org>  Sat, 07 May 2011 09:48:55 +0200

python-enthoughtbase (3.1.0-1) unstable; urgency=low

  * New upstream release
  * Convert to dh_python2 (Closes: #617003)

 -- Varun Hiremath <varun@debian.org>  Tue, 05 Apr 2011 23:45:59 -0400

python-enthoughtbase (3.0.6-1) experimental; urgency=low

  * New upstream release
  * d/copyright: Fix BSD license referencing
  * Drop patches/fix_exceptions.diff -- merged upstream

 -- Varun Hiremath <varun@debian.org>  Sun, 17 Oct 2010 21:24:43 -0400

python-enthoughtbase (3.0.5-2) unstable; urgency=low

  * Add patches/fix_exceptions.diff to fix Python string exceptions
    (Closes: #585289)
  * Remove Conflicts on mayavi2, python-enthought-traits - not needed
  * Bump Standards-Version to 3.9.1

 -- Varun Hiremath <varun@debian.org>  Wed, 11 Aug 2010 01:49:01 -0400

python-enthoughtbase (3.0.5-1) unstable; urgency=low

  * New upstream release

 -- Varun Hiremath <varun@debian.org>  Thu, 03 Jun 2010 01:47:57 -0400

python-enthoughtbase (3.0.4-1) unstable; urgency=low

  * New upstream release
  * Switch to source format 3.0
  * Bump Standards-Version to 3.8.4

 -- Varun Hiremath <varun@debian.org>  Sun, 28 Feb 2010 14:26:52 -0500

python-enthoughtbase (3.0.3-2) unstable; urgency=low

  * debian/control:
    - Add python-apptools to Depends (Closes: #560723)
    - Bump Standards-Version to 3.8.3

 -- Varun Hiremath <varun@debian.org>  Sun, 13 Dec 2009 00:02:26 -0500

python-enthoughtbase (3.0.3-1) unstable; urgency=low

  * New upstream release
  * Bump Standards-Version to 3.8.2
  * debian/copyright: fix lintian warnings

 -- Varun Hiremath <varun@debian.org>  Mon, 20 Jul 2009 02:52:45 -0400

python-enthoughtbase (3.0.2-1) unstable; urgency=low

  * New upstream release

 -- Varun Hiremath <varun@debian.org>  Fri, 27 Mar 2009 04:48:02 -0400

python-enthoughtbase (3.0.1-1) unstable; urgency=low

  [ Sandro Tosi ]
  * debian/control
    - switch Vcs-Browser field to viewsvn

  [ Varun Hiremath ]
  * New upstream release
  * debian/control: add python-setupdocs to Build-Depends

 -- Varun Hiremath <varun@debian.org>  Sun, 28 Dec 2008 22:36:01 -0500

python-enthoughtbase (3.0.0-1) experimental; urgency=low

  * Initial release

 -- Varun Hiremath <varun@debian.org>  Sun, 26 Oct 2008 01:27:12 -0400
